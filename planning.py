# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, timedelta
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, Id, If
from trytond.report import Report
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport
from trytond.transaction import Transaction
import copy
STATES = {'readonly': (Eval('state') != 'draft')}


class Period(Workflow, ModelSQL, ModelView):
    'Maintenance Period'
    __name__ = 'maintenance.period'
    name = fields.Char('Name Period', required=True, states=STATES)
    reference = fields.Char('Reference', required=True, states=STATES)
    start_date = fields.Date('Start Date', required=True, states=STATES)
    end_date = fields.Date('End Date', required=True, states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('closed', 'Closed'),
        ('cancel', 'Cancel'),
        ], 'State', readonly=True, required=True)

    @classmethod
    def __setup__(cls):
        super(Period, cls).__setup__()
        cls._order.insert(0, ('start_date', 'ASC'))
        # cls._constraints += [
        #     ('overlap_date', 'periods_overlaps'),
        #     ('check_dates', 'wrong_date'),
        # ]
        # cls._error_messages.update({
        #         'periods_overlaps': 'You can not have two overlapping periods!',
        #         'wrong_date': 'The start date is greater or equal than end date \n'
        #                       'or start date is smaller that today!'
        #         })
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'closed'),
            ('open', 'draft'),
            ('draft', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') == 'draft',
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'closed': {
                'invisible': Eval('state') != 'open',
            },
            'open': {
                'invisible': Eval('state') != 'draft',
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, records):
        pass

    def _get_last_date(self, equipment, line):
        pool = Pool()
        last_date = None
        last_plan_date = None
        Request = pool.get('maintenance.request_service')
        Forecast = pool.get('maintenance.schedule')

        requests = Request.search([
                ('equipment', '=', equipment),
                ('schedules.line', '=', line),
                ('state', '!=', 'cancel')
                ])

        forecasts = Forecast.search([
                ('equipment', '=', equipment),
                ('line', '=', line),
                ])

        if requests:
            for req in requests:
                if not last_date or req.effective_date > last_date:
                    last_date = req.effective_date
            if isinstance(last_date, datetime):
                last_date = last_date.date()

        if forecasts:
            for forecast in forecasts:
                if not last_plan_date or forecast.planning_date > last_plan_date:
                    last_plan_date = forecast.planning_date
            if not last_date or last_date < last_plan_date:
                last_date = last_plan_date
        return last_date

    def _get_plan_date(self, period, line, last_date, start_date=None):
        plan_date = []
        frecuency = timedelta(float(line.frecuency))
        if isinstance(last_date, datetime):
            last_date = last_date.date()

        if not last_date and start_date:
            last_date = start_date

        if not last_date or (last_date + frecuency) < self.start_date:
            last_date = self.start_date

        while (last_date + frecuency) <= period.end_date:
            last_date = last_date + frecuency
            plan_date.append(last_date)
        return plan_date

    @classmethod
    def check_dates(cls, plan_periods):
        """
        now = datetime.now().date()
        for period in plan_periods:
            if period.start_date >= period.end_date or period.start_date <= now:
                return False
        """
        return True

    @classmethod
    def overlap_date(cls, plan_periods):
        new_period = plan_periods[0]
        old_periods = cls.search([('id', '!=', new_period.id)])
        for old_period in old_periods:
            if (
                new_period.start_date <= old_period.start_date and \
                new_period.end_date >= old_period.start_date) \
                or (
                new_period.start_date >= old_period.start_date and \
                new_period.start_date <= old_period.end_date \
                ):
                return False
        return True

    @classmethod
    def create_forecast(cls, planning, period, start_date=None):
        pool = Pool()
        Forecast = pool.get('maintenance.schedule')

        lines_to_create = []
        for line in planning.lines:
            last_date = period._get_last_date(planning.equipment, line.code)
            plan_date = period._get_plan_date(period, line, last_date)
            if not plan_date:
                continue
            for new_date in plan_date:
                lines_to_create.append({
                    'plan_period': period.id,
                    'planning': planning.id,
                    'equipment': planning.equipment,
                    'description': line.description,
                    'line': line.id,
                    'code': line.code.id,
                    'last_date': last_date,
                    'work_forecast_hours': line.work_forecast_hours,
                    'planning_date': new_date,
                    'state': 'open',
                    })
        Forecast.create(lines_to_create)


class Planning(ModelSQL, ModelView):
    'Maintenance Planning'
    __name__ = 'maintenance.planning'
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
            required=True)
    total_cost_estimate = fields.Function(
        fields.Numeric('Total Cost Estimate'), 'get_total_cost_estimate')
    type_activity = fields.Many2One('maintenance.type_activity',
            'Type Activity', required=True)
    notes = fields.Text('Notes')
    party = fields.Many2One('party.party', 'Party')
    lines = fields.One2Many('maintenance.planning.line', 'planning',
            'Lines')

    @classmethod
    def __setup__(cls):
        super(Planning, cls).__setup__()

    def get_total_cost_estimate(self, name=None):
        total_amount = 0
        total_amount = sum(line.cost_estimate for line in self.lines)
        return total_amount


    @staticmethod
    def default_type_activity():
        TypeActivity = Pool().get('maintenance.type_activity')
        activities = TypeActivity.search([
            ('activity', '=', 'preventive'),
        ])
        if activities:
            return activities[0].id


class PlanningLine(ModelSQL, ModelView):
    'Maintenance Planning Line'
    __name__ = 'maintenance.planning.line'
    _rec_name = 'name'
    planning = fields.Many2One('maintenance.planning', 'Planning',
            ondelete='CASCADE', required=True)
    name = fields.Char('Name', required=True)
    code = fields.Many2One('maintenance.planning_code', 'Code',
            required=True)
    description = fields.Text('Description', required=True)
    frecuencies_lines = fields.One2Many('maintenance.planning.line.frecuency',
            'line', 'Frecuencies Lines')
    work_forecast_hours = fields.Numeric('Work Forecast Hours')
    cost_estimate = fields.Numeric('Cost Estimate', digits=(16, 2))
    party = fields.Many2One('party.party', 'Party')
    last_maintenance = fields.Date('Last Maintenance')
    days_limit_schedule = fields.Integer('Days Limit Schedule')

    @classmethod
    def __setup__(cls):
        super(PlanningLine, cls).__setup__()
        cls._buttons.update({
                'create_frecuencies': {
                    'invisible': Eval('frecuencies_lines', [0]),
                },
        })

    @staticmethod
    def default_cost_estimate():
        return 0

    @fields.depends('code', 'description', 'frecuencies_lines')
    def on_change_code(self):
        if not self.code:
            return
        self.name = self.code.name
        if self.code.description:
            self.description = self.code.description

    @classmethod
    @ModelView.button
    def create_frecuencies(cls, lines):
        values_to_create = []
        for line in lines:
            if line.code:
                for f in line.code.frecuencies:
                    val = {
                        'frecuency_kind': f.frecuency_kind.id,
                        'value': f.value,
                    }
                    values_to_create.append(val)
            if not values_to_create:
                return
        cls.write([line], {
                'frecuencies_lines': [('create', values_to_create)]
        })


class FrecuencyKind(ModelSQL, ModelView):
    'Frecuency Kind'
    __name__ = 'maintenance.planning.frecuency_kind'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    uom_frecuency = fields.Many2One('product.uom', 'UoM Frecuency',
        required=True, domain=[
            If(Eval('source_data') == 'clock',
                ('id', 'in', [
                        Id('product', 'uom_hour'),
                        Id('product', 'uom_day'),
                ]), ())
                ], depends=['clock'])
    source_data = fields.Selection([
            ('clock', 'Clock'),
            ], 'Source Data', required=True)

    @classmethod
    def __setup__(cls):
        super(FrecuencyKind, cls).__setup__()

    @staticmethod
    def default_source_data():
        return 'clock'


class LineFrecuency(ModelSQL, ModelView):
    'Line Frecuency'
    __name__ = 'maintenance.planning.line.frecuency'
    line = fields.Many2One('maintenance.planning.line', 'Line',
            ondelete='CASCADE', required=True)
    frecuency_kind = fields.Many2One('maintenance.planning.frecuency_kind',
            'Frecuency Kind', required=True)
    value = fields.Integer('Value', required=True)
    note = fields.Text('Notes')


class PlanningCode(ModelSQL, ModelView):
    'Maintenance Planning Code'
    __name__ = 'maintenance.planning_code'
    _rec_name = 'code'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    description = fields.Text('Description')
    type_ = fields.Selection([
            ('root', 'Root'),
            ('normal', 'Normal'),
            ], 'Type', required=True)
    parent = fields.Many2One('maintenance.planning_code', 'Parent',
            domain=[
                ('type_', '=', 'root')
            ], states={
                'invisible': Eval('type_') == 'root',
            })
    childs = fields.One2Many('maintenance.planning_code', 'parent',
            'Children', states={
                'invisible': Eval('type_') != 'root',
            })
    frecuencies = fields.One2Many('maintenance.planning_code.frecuency_kind',
            'code', 'Default Frecuencies', states={
                'invisible': Eval('type_') == 'root',
            })

    @staticmethod
    def default_type_():
        return 'normal'


class PlanningCodeFrecuencyKind(ModelSQL, ModelView):
    'Planning Code - Planning Frecuency Kind'
    __name__ = 'maintenance.planning_code.frecuency_kind'
    code = fields.Many2One('maintenance.planning_code',
            'Code', required=True, ondelete='CASCADE')
    frecuency_kind = fields.Many2One('maintenance.planning.frecuency_kind',
            'Frecuency Kind', required=True)
    value = fields.Integer('Value', required=False)


class CreateForecastStart(ModelView):
    'Create Forecast Start'
    __name__ = 'maintenance.create_forecast.start'
    period = fields.Many2One('maintenance.period', 'Period',
            domain=[
                ('state', '=', 'open'),
            ], required=True)
    start_date = fields.Date('Start Date')


class CreateForecast(Wizard):
    'Create Forecast'
    __name__ = 'maintenance.create_forecast'
    start = StateView('maintenance.create_forecast.start',
        'maintenance.create_forecast_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        Planning = Pool().get('maintenance.planning')
        Period = Pool().get('maintenance.period')
        period = Period(self.start.period)
        ids = Transaction().context['active_ids']
        plannings = Planning.browse(ids)
        for plan in plannings:
            start_date = self.start.start_date
            Period.create_forecast(plan, period, start_date=start_date)
        return 'end'


class AddLastMaintenanceDateStart(ModelView):
    'Add Last Maintenance Date Start'
    __name__ = 'maintenance.add_last_maintenance_date.start'
    maintenance_date = fields.Date('Maintenance Date', required=True)


class AddLastMaintenanceDate(Wizard):
    'Add Last Maintenance Date'
    __name__ = 'maintenance.add_last_maintenance_date'
    start = StateView('maintenance.add_last_maintenance_date.start',
        'maintenance.add_last_maintenance_date_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        Planning = Pool().get('maintenance.planning')
        ids = Transaction().context['active_ids']
        plannings = Planning.browse(ids)
        for planning in plannings:
            PlanningLine.write(list(planning.lines), {
                'last_maintenance': self.start.maintenance_date
            })
        return 'end'


class PlanningForecastStart(ModelView):
    'Planning Forecast Start'
    __name__ = 'maintenance.print_planning_forecast.start'
    date = fields.Date('Start Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PlanningForecast(Wizard):
    'Planning Forecast'
    __name__ = 'maintenance.print_planning_forecast'
    start = StateView('maintenance.print_planning_forecast.start',
            'maintenance.print_planning_forecast_start_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print', 'print_', 'tryton-print', default=True),
                ])
    print_ = StateReport('maintenance.planning_forecast_report')

    def do_print_(self, action):
        company = self.start.company
        data = {
                'date': self.start.date,
                'company': company.id,
                }
        return action, data

    def transition_print_(self):
        return 'end'


class PlanningForecastReport(Report):
    __name__ = 'maintenance.planning_forecast_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(PlanningForecastReport, cls).get_context(records, header, data)
        MAX_DAYS = 91
        pool = Pool()
        Company = pool.get('company.company')
        Planning = pool.get('maintenance.planning')
        costs = {}
        all_days = {}
        all_days_convert = {}
        for nd in range(1, MAX_DAYS):
            # day_n = 'day' + str((nd + 1))
            t_date = data['date'] + timedelta(nd)
            data[nd] = t_date
            costs[t_date] = {
                'date': t_date,
                'costs': 0
            }
            all_days[nd] = None
            all_days_convert[t_date] = {nd: 0, 'cost': ''}
        print(all_days_convert)
        date_limit = data['date'] + timedelta(MAX_DAYS)
        planning = Planning.search([])

        records = {}
        # total_executed = []
        # total_no_executed = []
        # rate_full_filment = 0
        # sum_dates_deviation = 0.0
        for pln in planning:
            records[pln.equipment] = {
                'equipment': pln.equipment.rec_name,
                'code': pln.equipment.code,
            }
            tmp_dates = copy.deepcopy(all_days_convert)
            for line in pln.lines:
                last_maintenance = pln.lines[0].last_maintenance
                if line.frecuencies_lines:
                    for day_index in range(0, MAX_DAYS, line.frecuencies_lines[0].value + 1):
                        last_maintenance = last_maintenance + timedelta(day_index)
                        # print(last_maintenance, last_maintenance in data)
                        if data[90] >= last_maintenance and last_maintenance > data['date']:
                            # tmp_dates[last_maintenance]['cost'] += line.cost_estimate
                            tmp_dates[last_maintenance]['cost'] = 'X'
                        # if costs[day_index]['date'] == last_maintenance:
                            # records[pln.equipment]['costs'][day_index]['costs'] += line.cost_estimate
                        # elif costs[90]['date'] < last_maintenance:
                            # break
            records[pln.equipment]['costs'] = list(tmp_dates.values())
        print(records)
        report_context['records'] = records.values()
        # report_context['total_schedule'] = total_schedule
        # report_context['total_executed'] = sum(total_executed)
        # report_context['total_no_executed'] = sum(total_no_executed)
        # report_context['rate_fullfilment'] = rate_fullfilment
        # report_context['dates_deviation_avg'] = dates_deviation_avg
        report_context['date'] = data['date']
        report_context['company'] = Company(data['company']).party.name
        return report_context
