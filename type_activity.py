# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class TypeActivity(ModelSQL, ModelView):
    "Type Activity"
    __name__ = 'maintenance.type_activity'
    name = fields.Char('Name', required=True)
    activity = fields.Selection([
                ('corrective', 'Corrective'),
                ('preventive', 'Preventive'),
                ('calibration', 'Calibration'),
                ('assembly', 'Assembly'),
                ('other', 'Other'),
                ], 'Activity', required=True)
