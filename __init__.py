# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import type_activity
from . import location
from . import equipment
from . import request_service
from . import planning
from . import schedule
from . import configuration
from . import stock
from . import inspection
from . import dash


def register():
    Pool.register(
        configuration.Configuration,
        equipment.Brand,
        equipment.Classification,
        equipment.EquipmentCategory,
        equipment.EquipmentKind,
        equipment.Equipment,
        equipment.EquipmentOperationCard,
        equipment.MaintenanceEquipmentPart,
        equipment.MaintenanceEquipmentProduct,
        equipment.CheckListEquipment,
        equipment.VehiclePoolStart,
        equipment.VehicleTechMechanical,
        equipment.EquipmentInsurance,
        equipment.EventEquipment,
        # stock.Move,
        stock.ShipmentInternal,
        location.MaintenanceLocation,
        type_activity.TypeActivity,
        planning.PlanningCode,
        planning.Period,
        planning.Planning,
        planning.PlanningLine,
        planning.FrecuencyKind,
        planning.PlanningCodeFrecuencyKind,
        planning.LineFrecuency,
        planning.CreateForecastStart,
        planning.AddLastMaintenanceDateStart,
        planning.PlanningForecastStart,
        request_service.RequestService,
        request_service.RequestServiceCost,
        request_service.CheckListService,
        request_service.InvoiceLineRequestService,
        request_service.RequestServiceCostsStart,
        # request_service.CreateMaintenanceMoveStart,
        request_service.ServiceRequestsSummaryStart,
        schedule.Schedule,
        schedule.ScheduleForecastStart,
        inspection.InspectionTemplate,
        inspection.InspectionTemplateLine,
        inspection.Inspection,
        inspection.InspectionLine,
        inspection.InspectionGroup,
        dash.DashApp,
        dash.AppMaintenance,
        module='maintenance', type_='model')
    Pool.register(
        planning.CreateForecast,
        planning.AddLastMaintenanceDate,
        planning.PlanningForecast,
        schedule.CreateScheduling,
        schedule.ScheduleForecast,
        equipment.VehiclePool,
        request_service.RequestServiceCosts,
        request_service.ServiceRequestsSummary,
        # request_service.CreateMaintenanceMove,
        module='maintenance', type_='wizard')
    Pool.register(
        inspection.InspectionReport,
        request_service.RequestServiceReport,
        request_service.RequestServiceCostsReport,
        request_service.ServiceRequestsSummaryReport,
        schedule.ScheduleForecastReport,
        equipment.VehiclePoolReport,
        equipment.EquipmentReport,
        planning.PlanningForecastReport,
        module='maintenance', type_='report')
