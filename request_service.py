# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from datetime import date, datetime, timedelta
from decimal import Decimal

from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import And, Equal, Eval, Get, If, In, Not, Or
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

STATES_USER = {
    'readonly': (Eval('state') != 'open'),
}

_STATES = Not(In(Eval('state'), ['approved', 'assigned', 'waiting_approval']))

STATES_ADMIN = {
    'readonly': _STATES,
}
_ZERO = Decimal(0)


class RequestService(Workflow, ModelSQL, ModelView):
    "Request Service"
    __name__ = 'maintenance.request_service'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES_USER, domain=[('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0))])
    request_date = fields.DateTime('Request Date', states=STATES_ADMIN,
            required=True)
    repair_time = fields.Numeric('Repair Time', help="In hours",
        states=STATES_ADMIN)
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
         states={
             'required': Eval('kind') == 'equipment',
         })
    location = fields.Many2One('maintenance.location', 'Location',
        depends=['equipment'], states={'readonly': Or(
            (Eval('create_uid') != Eval('_user', '0')),
            (Eval('state') != 'open')),
        })
    description = fields.Text('Description', required=True)
    type_activity = fields.Many2One('maintenance.type_activity',
        'Type Activity', states=STATES_ADMIN)
    action = fields.Text('Action', states=STATES_ADMIN)
    code = fields.Many2One('maintenance.planning_code', 'Code',
        states={
            'readonly': True,
            'invisible': Eval('kind') != 'equipment',
        })
    period = fields.Char('Period', readonly=True, states=STATES_ADMIN)
    assigned_to = fields.Many2One('party.party', 'Party', states=STATES_ADMIN)
    type_service = fields.Selection([
        ('internal', 'Internal'),
        ('external', 'External'),
        ('both', 'Both'),
        ], 'Type')
    type_service_string = type_service.translated('type_service')
    priority = fields.Selection([
        ('urgent', 'Urgent'),
        ('important', 'Important'),
        ('low', 'Low'),
        ], 'Priority', required=True, states=STATES_USER)
    kind = fields.Selection([
        ('equipment', 'Equipment'),
        # ('room', 'Room'),
        ], 'Kind', required=False)
    priority_string = priority.translated('priority')
    effective_date = fields.DateTime('Effective Datetime',
        states={
            'readonly': Not(In(Eval('state'), ['approved', 'assigned', 'waiting_approval'])),
            'required': Equal(Eval('state'), 'done'),
        })
    notes = fields.Char('Notes', states=STATES_ADMIN)
    efficacy = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No'),
        (None, ''),
        ], 'Efficacy', states={
            'readonly': Or(
                    (Eval('create_uid') != Eval('_user', '0')),
                    (Eval('state') != 'done')),
            'required': Eval('state') == 'marked',
        })
    efficacy_string = efficacy.translated('efficacy')
    department = fields.Many2One('company.department', 'Department',
        states=STATES_USER)
    state = fields.Selection([
        ('open', 'Open'),
        ('assigned', 'Assigned'),
        ('cancelled', 'Cancelled'),
        ('waiting_approval', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('no_approved', 'No Approved'),
        ('done', 'Done'),
        ('marked', 'Marked'),
    ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    check_list = fields.One2Many('maintenance.check_list',
        'request', 'Check List', states=STATES_ADMIN)
    approved_notes = fields.Text('Approved Notes', states={
        'readonly': Eval('state') != 'waiting_approval',
    })
    odometer = fields.Integer('Odometer', states=STATES_ADMIN)
    # TODO fix sale relation is function
    # moves = fields.One2Many('stock.move', 'sale', 'Moves', states=STATES_ADMIN,
    #     readonly=True)
    invoices_lines = fields.Many2Many(
        'invoice.line-maintenance.request_service',
        'request_service', 'invoice_line', 'Invoices Lines',
        domain=[
            ('invoice.type', '=', 'in'),
            ('invoice.state', 'not in', ['draft', 'cancelled']),
        ])
    total_cost = fields.Function(fields.Numeric('Total Cost',
        digits=(16, 2)), 'get_total_cost')
    origin = fields.Reference('Origin', selection='get_origin')
    shipments = fields.One2Many('stock.shipment.internal', 'origin',
        'Shipments', readonly=True)
    # moves = fields.One2Many('stock.move', 'origin',
    #     'Moves', readonly=True)
    piece = fields.Char('Piece')
    revised_by = fields.Many2One('res.user', 'Revised By', states={'readonly': True})
    costs = fields.One2Many('maintenance.request_service.cost',
        'request_service', 'Costs', states=STATES_ADMIN)

    @classmethod
    def __setup__(cls):
        super(RequestService, cls).__setup__()
        cls._order.insert(0, ('request_date', 'DESC'))
        cls._transitions |= set((
                ('open', 'assigned'),
                ('open', 'cancelled'),
                ('assigned', 'open'),
                ('assigned', 'done'),
                ('assigned', 'waiting_approval'),
                ('waiting_approval', 'no_approved'),
                ('waiting_approval', 'approved'),
                ('approved', 'done'),
                ('done', 'marked'),
                ))
        cls._buttons.update({
            'open': {
                'invisible': Eval('state') == 'open',
                },
            'assign': {
                'invisible': Eval('state') != 'open',
                },
            'cancel': {
                'invisible': Eval('state') != 'open',
                },
            'waiting_approval': {
                'invisible': Eval('state') != 'assigned',
                },
            'approved': {
                'invisible': Eval('state') != 'waiting_approval',
                },
            'no_approved': {
                'invisible': Eval('state') != 'waiting_approval',
                },
            'done': {
                'invisible': And(
                    Eval('state') != 'assigned',
                    Eval('state') != 'approved',
                )},
            'marked': {
                'invisible': Eval('state') != 'done',
                },
            'create_internal_shipment': {
                'invisible': Eval('state') != 'assigned',
                },
            })

    @staticmethod
    def _get_origin():
        "Return list of Model names for origin Reference"
        return ['maintenance.schedule']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
                ('model', 'in', models),
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    def get_shipments(self, name):
        shipments = []
        return shipments

    def get_piece(self, name=None):
        origin = self.origin
        if origin and str(origin).split(',')[0] == 'maintenance.schedule':
            return origin.line.name

    def search_shipments(self):
        shipments = []
        return shipments

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_state():
        return 'open'

    @staticmethod
    def default_request_date():
        return datetime.now()

    @staticmethod
    def default_priority():
        return 'low'

    @staticmethod
    def default_kind():
        return 'equipment'

    @staticmethod
    def default_type_service():
        return 'internal'

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('assigned')
    def assign(cls, records):
        cls.set_number(records)
        # for rec in records:
        #     rec.send_email('request_service_email')

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting_approval')
    def waiting_approval(cls, records):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Shipment.delete([shipment for record in records
                         for shipment in record.shipments
                         if not shipment.moves or
                         shipment.state == 'cancelled'])
        Shipment.wait([shipment for record in records
                       for shipment in record.shipments])

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approved(cls, records):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Shipment.assign([shipment for record in records
                         for shipment in record.shipments])

    @classmethod
    @ModelView.button
    @Workflow.transition('no_approved')
    def no_approved(cls, records):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Shipment.delete([shipment for record in records
                         for shipment in record.shipments
                         if not shipment.moves])
        Shipment.cancel([shipment for record in records
                         for shipment in record.shipments])

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        shipments = []
        for record in records:
            cls._close_forecast(record)
            origin = record.origin
            record.revised_by = Transaction().user
            record.save()
            if origin and str(origin).split(',')[0] == 'maintenance.schedule':
                origin.line.last_maintenance = record.effective_date.date()
                origin.line.save()
                origin.save()
            shipments.extend(record.shipments)
        Shipment.done(shipments)

    @classmethod
    @ModelView.button
    @Workflow.transition('marked')
    def marked(cls, records):
        pass

    def send_email(self, name):
        pool = Pool()
        config = pool.get('maintenance.configuration')(1)
        Template = pool.get('email.template')
        # Activity = pool.get('email.activity')
        # if not self.party or not self.party.email:
        #     return
        # email = self.company.party.email
        template = getattr(config, name)
        # template = config.
        email = template.from_email
        if not template:
            return

        template.subject = f'{template.subject} No. {self.number}'
        if email:
            Template.send(template, self, email, attach=True)
            # Activity.create([{
            #     'template': template.id,
            #     'origin': str(self),
            #     'status': 'sended',
            # }])

    def get_total_cost(self, name=None):
        res = _ZERO
        for iline in self.costs:
            res += iline.amount
        for shipment in self.shipments:
            for move in shipment.moves:
                res += (Decimal(move.quantity) * move.product.cost_price)
        return res

    @classmethod
    def set_number(cls, requests):
        """
        Fill the number field with the request sequence
        """
        pool = Pool()
        Config = pool.get('maintenance.configuration')
        config = Config(1)

        for request in requests:
            if request.number:
                continue
            if not config.maintenance_sequence:
                continue
            number = config.maintenance_sequence.get()
            cls.write([request], {'number': number})

    @classmethod
    def set_created_by(cls, requests):
        """
        Fill the created_by field with the request
        """
        pool = Pool()
        User = pool.get('res.user')
        user = User.search(Transaction().user)

        for request in requests:
            cls.write(request.id, {
                'created_by': user.id,
                })

    @fields.depends('equipment', 'location')
    def on_change_equipment(self):
        self.location = None
        if self.equipment:
            if self.equipment.location:
                self.location = self.equipment.location.id

    @classmethod
    def _close_forecast(cls, record):
        Schedule = Pool().get('maintenance.schedule')
        if record.origin:
            if isinstance(record.effective_date, datetime):
                eff_date = record.effective_date.date()
            Schedule.write([record.origin], {
                    'state': 'done',
                    'exec_date': eff_date,
            })

    @classmethod
    # @ModelView.button_action('maintenance.act_create_internal_shipment')
    def create_internal_shipment(cls, requests):
        for request in requests:
            request._create_internal_shipment()

    def _create_internal_shipment(self):
        ShipmentInternal = Pool().get('stock.shipment.internal')
        config = Pool().get('maintenance.configuration')(1)
        today = date.today()
        values = {
            'number': self.number,
            'planned_date': today,
            'planned_start_date': today,
            'origin': str(self),
        }
        if self.equipment.warehouse:
            values['from_location'] = self.equipment.warehouse.storage_location.id
        if config.to_location_request:
            values['to_location'] = config.to_location_request.storage_location.id
        ShipmentInternal.create([values])


class RequestServiceReport(Report):
    "Request Service Report"
    __name__ = 'maintenance.request_service'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class CheckListService(ModelSQL, ModelView):
    "Check List Service"
    __name__ = 'maintenance.check_list'

    element = fields.Char('Element', required=True)
    quantity = fields.Numeric('Quantity', required=True)
    checked = fields.Boolean('Checked')
    notes = fields.Char('Notes')
    request = fields.Many2One('maintenance.request_service', 'Request')

    @classmethod
    def __setup__(cls):
        super(CheckListService, cls).__setup__()


class InvoiceLineRequestService(ModelSQL):
    "Invoice Line - Request Service"
    __name__ = 'invoice.line-maintenance.request_service'
    _table = 'invoice_line_maintenance_request_service'

    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
        required=True, ondelete='CASCADE')
    request_service = fields.Many2One('maintenance.request_service',
        'Request Service', required=True, ondelete='CASCADE')


class RequestServiceCost(ModelSQL, ModelView):
    "Request Service Cost"
    __name__ = 'maintenance.request_service.cost'
    request_service = fields.Many2One('maintenance.request_service',
        'Request Service', required=True, ondelete='CASCADE')
    number = fields.Integer('Number', required=True)
    effective_date = fields.Date('Effec. Date', required=True)
    concept = fields.Char('Concept', required=True)
    amount = fields.Numeric('Amount', required=True)
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
                                   domain=[('invoice.type', '=', 'out')])
    notes = fields.Text('Notes')

    @fields.depends('effective_date', 'concept', 'amount', 'invoice_line',
                    'notes')
    def on_change_invoice_line(self):
        if self.invoice_line:
            self.effective_date = self.invoice_line.invoice.invoice_date
            self.concept = self.invoice_line.product.name
            self.amount = self.invoice_line.amount
            self.notes = self.invoice_line.note


class RequestServiceCostsStart(ModelView):
    "Request Service Costs Start"
    __name__ = 'maintenance.request_service_costs.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)
    type_activity = fields.Many2One('maintenance.type_activity',
        'Type Activity')
    state = fields.Selection([
        (None, ''),
        ('open', 'Open'),
        ('assigned', 'Assigned'),
        ('cancelled', 'Cancelled'),
        ('waiting_approval', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('no_approved', 'No Approved'),
        ('done', 'Done'),
        ('marked', 'Marked'),
    ], 'State')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class RequestServiceCosts(Wizard):
    "Request Service Costs"
    __name__ = 'maintenance.request_service_costs'
    start = StateView(
        'maintenance.request_service_costs.start',
        'maintenance.request_service_costs_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('maintenance.request_service_costs_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'type_activity': self.start.type_activity and self.start.type_activity.id,
            # 'state': self.start.state,
            'fiscalyear': self.start.fiscalyear.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class RequestServiceCostsReport(Report):
    "Request Service Costs Report"
    __name__ = 'maintenance.request_service_costs_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Request = pool.get('maintenance.request_service')
        TypeActivity = pool.get('maintenance.type_activity')
        FiscalYear = pool.get('account.fiscalyear')
        type_activities = TypeActivity.search([])
        activities = {
            activity.name:
            {
                'activity': activity.name,
                'quantity': 0,
                'amount': 0,
                'yes': 0, 'no': 0, None: 0,
            } for activity in type_activities
        }
        company = Company(data['company'])
        records = []
        year = []
        months_dict = {key: 0 for key in range(1, 13)}
        months = months_dict.copy()
        domain = []
        fiscalyear = FiscalYear(data['fiscalyear'])
        if data['type_activity']:
            domain.append(('type_activity', '=', data['type_activity']))
        start = datetime.combine(fiscalyear.start_date, datetime.min.time())
        domain.append(('request_date', '>=', start))
        end = datetime.combine(fiscalyear.end_date, datetime.max.time())
        domain.append(('request_date', '<=', end))
        domain.append(('state', '=', 'done'))
        services = Request.search(domain)
        _records = {}
        for service in services:
            if service.equipment not in _records:
                _records[service.equipment] = {
                    'indicator': service.equipment.rec_name,
                    'months': months_dict.copy(),
                    # 'forecast': cls.create_forecast(service.equipment, months_dict.copy(), fiscalyear.start_date),
                    'total': 0,
                    'activities': copy.deepcopy(activities),
                    'total_activities': 0,
                }
            _amount = 0
            # for cost in service.costs:
            #     _month = cost.effective_date.month
            #     _amount = cost.amount
            #     _records[service.equipment]['months'][_month] += _amount
            #     _records[service.equipment]['total'] += _amount
            #     months[_month] += _amount
            #     year.append(_amount)
            _month = service.effective_date.month
            _amount = service.total_cost
            _records[service.equipment]['months'][_month] += _amount
            _records[service.equipment]['total'] += _amount
            months[_month] += _amount
            year.append(_amount)
            type_activity = service.type_activity.name
            _records[service.equipment]['activities'][type_activity]['quantity'] += 1
            _records[service.equipment]['activities'][type_activity]['amount'] += service.total_cost
            _records[service.equipment]['activities'][type_activity][service.efficacy] += 1
            _records[service.equipment]['total_activities'] += 1
        total_planing = months_dict.copy()
        records = _records.values()
        # for record in records:
        #     for index in range(1, 13):
        #         total_planing[index] += record['forecast'][index]
        report_context['_records'] = records
        report_context['total_planing'] = total_planing
        report_context['company'] = company
        report_context['data'] = months
        report_context['year'] = start.year
        report_context['type_activity'] = TypeActivity(data['type_activity']).name if data['type_activity'] else ''
        report_context['total_year'] = sum(year)
        return report_context

    @classmethod
    def create_activity_type_dictionary(cls, activities):
        activities = {activity.name: {'activity': activity.name} for activity in activities}
        return

    @classmethod
    def create_forecast(cls, equipment, month_dict, today):
        Planning = Pool().get('maintenance.planning')
        planning = Planning.search([('equipment', '=', equipment.id)])
        current_year = today.year
        if len(planning) > 0:
            for line in planning[0].lines:
                last_maintenance = line.last_maintenance
                if line.frecuencies_lines:
                    max_days = (date(year=current_year, month=12, day=31) - last_maintenance).days
                    for day_index in range(0, max_days, line.frecuencies_lines[0].value):
                        last_maintenance = last_maintenance + timedelta(day_index)
                        # print(last_maintenance, last_maintenance in data)
                        month = last_maintenance.month
                        if current_year == last_maintenance.year:
                            month_dict[month] += line.cost_estimate
        return month_dict


class ServiceRequestsSummaryStart(ModelView):
    "Request Service Costs Start"
    __name__ = 'maintenance.service_requests_summary.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ServiceRequestsSummary(Wizard):
    "Request Service Costs"
    __name__ = 'maintenance.service_requests_summary'
    start = StateView(
        'maintenance.service_requests_summary.start',
        'maintenance.service_requests_summary_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('maintenance.service_requests_summary_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ServiceRequestsSummaryReport(Report):
    "Request Service Costs Report"
    __name__ = 'maintenance.service_requests_summary_report'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        report_context = super().get_context(records, header, data)
        Company = pool.get('company.company')
        company = Company(data['company'])
        FiscalYear = pool.get('account.fiscalyear')
        fiscalyear = FiscalYear(data['fiscalyear'])
        to_date = fiscalyear.end_date + timedelta(days=1)
        months = {key: 0 for key in range(1, 13)}
        _states = cls.get_data(fiscalyear.start_date, to_date, 'state')
        total_states = cls.get_totals(_states, months.copy())
        _types = cls.get_data(fiscalyear.start_date, to_date, 'type_service')
        total_types = cls.get_totals(_types, months.copy())
        result_activities = cls.get_data_by_activity(fiscalyear.start_date, to_date)
        total_activities = cls.get_totals(result_activities, months.copy())
        total_executes = cls.get_executes(_states, total_states, months.copy())
        report_context['total_states'] = total_states
        report_context['executes'] = total_executes
        report_context['total_types'] = total_types
        report_context['total_activities'] = total_activities
        report_context['company'] = company
        report_context['year'] = fiscalyear.end_date.year
        report_context['states'] = _states
        report_context['type_service'] = _types
        report_context['activities'] = result_activities
        return report_context

    @classmethod
    def get_data(cls, from_date, to_date, kind):
        cursor = Transaction().connection.cursor()
        sums_cols = [
            f"""SUM(CASE WHEN DATE_PART('month', mt.request_date) = {lap} THEN 1 ELSE 0 END) AS "{lap!s}"
            """
            for lap in range(1, 13)
        ]

        query_states = f"""
            SELECT
              mt.{kind},
              {",".join(sums_cols)}
            FROM maintenance_request_service as mt
            WHERE request_date>='{from_date}' AND request_date<'{to_date}'
            GROUP BY mt.{kind}
        """
        cursor.execute(query_states)
        return cursor.fetchall()

    @classmethod
    def get_data_by_activity(cls, from_date, to_date):
        cursor = Transaction().connection.cursor()
        sums_cols = [
            f"""SUM(CASE WHEN DATE_PART('month', mt.request_date) = {lap} THEN 1 ELSE 0 END) AS "{lap!s}"
            """
            for lap in range(1, 13)
        ]

        query_states = f"""
            SELECT
              ta.name as activity,
              {",".join(sums_cols)}
            FROM maintenance_request_service as mt
            INNER JOIN maintenance_type_activity ta
            ON ta.id = mt.type_activity
            WHERE request_date>='{from_date}' AND request_date<'{to_date}'
            GROUP BY ta.name
            """
        cursor.execute(query_states)
        return cursor.fetchall()

    @classmethod
    def get_totals(cls, records, months):
        result = {}
        result['total'] = 0
        result.update(months)
        for record in records:
            for rec in range(1, 13):
                result[record[0]] = result.get(record[0], 0) + record[rec]
                result[rec] += record[rec]
                result['total'] += record[rec]
        return result

    @classmethod
    def get_executes(cls, states, totals, months):
        result = {}
        result['total'] = 0
        result.update(months)
        for index in range(0, len(states)):
            if states[index][0] == 'done':
                for id in range(1, 13):
                    result[id] = (states[index][id] / totals[id]) * 100 if states[index][id] > 0 else 0
                    result['total'] += states[index][id]
                result['total'] = (result['total'] / totals['total']) * 100 if totals['total'] > 0 else 0
                break
        return result



# class MaintenanceCostControlStart(ModelView):
#     "Maintenance Cost Control Start"
#     __name__ = 'maintenance.maintenance_cost_control.start'

#     @staticmethod
#     def default_company():
#         return Transaction().context.get('company')


# class MaintenanceCostControl(Wizard):
#     "Request Service Costs"
#     __name__ = 'maintenance.maintenance_cost_control'
#     start = StateView(
#         'maintenance.service_requests_summary.start',
#         'maintenance.service_requests_summary_start_view_form', [
#             Button('Cancel', 'end', 'tryton-cancel'),
#             Button('Print', 'print_', 'tryton-print', default=True),
#         ])
#     print_ = StateReport('maintenance.service_requests_summary_report')

#     def do_print_(self, action):
#         data = {
#             'company': self.start.company.id,
#             'fiscalyear': self.start.fiscalyear.id,
#         }
#         return action, data

#     def transition_print_(self):
#         return 'end'


# class MaintenanceCostControlReport(Report):
#     "Maintenance Cost Control Report"
#     __name__ = 'maintenance.maintenance_cost_control_report'

#     @classmethod
#     def get_context(cls, records, header, data):

# class CreateMaintenanceMoveStart(ModelView):
#     "Create Maintenance Move Start"
#     __name__ = 'maintenance.create_maintenance_move.start'
#     product = fields.Many2One('product.product', 'Product',
#                               domain=[('type', '!=', 'service')], required=True)
#     quantity = fields.Float('Quantity', required=True)
#     product_uom_category = fields.Function(
#         fields.Many2One(
#             'product.uom.category', "Product UoM Category",
#             help="The category of Unit of Measure for the product."),
#         'on_change_with_product_uom_category')
#     unit = fields.Many2One('product.uom', "Unit", required=True)

#     @fields.depends('product', 'unit')
#     def on_change_product(self):
#         if self.product:
#             default_uom = self.product.default_uom
#             if (not self.unit
#                     or self.unit.category != default_uom.category):
#                 self.unit = default_uom

#     @fields.depends('product')
#     def on_change_with_product_uom_category(self, name=None):
#         return self.product.default_uom_category if self.product else None

#     @staticmethod
#     def default_unit():
#         return Transaction().context.get('company')


# class CreateMaintenanceMove(Wizard):
#     "Create Maintenance Move"
#     __name__ = 'maintenance.create_maintenance_move'
#     start = StateView('maintenance.create_maintenance_move.start',
#         'maintenance.create_maintenance_move_start_view_form', [
#             Button('Cancel', 'end', 'tryton-cancel'),
#             Button('Ok', 'accept', 'tryton-ok', default=True),
#         ])
#     accept = StateTransition()

#     def transition_accept(self):
#         pool = Pool()
#         config = Pool().get('maintenance.configuration')(1)
#         Move = pool.get('stock.move')
#         today = date.today()
#         active_id = Transaction().context['active_id']
#         request_service = pool.get('maintenance.request_service')(active_id)
#         warehouse = request_service.equipment.warehouse
#         move = {
#                 'product': self.start.product,
#                 'quantity': self.start.quantity,
#                 'from_location': warehouse.storage_location.id,
#                 'to_location': warehouse.lost_found_location.id,
#                 'origin': str(request_service),
#                 'effective_date': today,
#                 'unit': self.start.unit.id,
#             }
#         Move.create([move])
#         return 'end'
