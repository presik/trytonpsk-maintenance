
-- SELECT mt.id AS pid, request_date FROM maintenance_request_service AS mt;
SELECT mt.id AS pid, DATE_PART('month', mt.request_date) AS _month FROM maintenance_request_service AS mt;




SELECT
  mt.state AS state,
  SUM(CASE WHEN DATE_PART('month', mt.request_date) = 1 THEN 1 ELSE 0 END) AS "month1",
  SUM(CASE WHEN DATE_PART('month', mt.request_date) = 2 THEN 1 ELSE 0 END) AS "month2",
  SUM(CASE WHEN DATE_PART('month', mt.request_date) = 3 THEN 1 ELSE 0 END) AS "month3",
  SUM(CASE WHEN DATE_PART('month', mt.request_date) = 4 THEN 1 ELSE 0 END) AS "month4",
  SUM(CASE WHEN DATE_PART('month', mt.request_date) = 5 THEN 1 ELSE 0 END) AS "month5",
  SUM(CASE WHEN DATE_PART('month', mt.request_date) = 6 THEN 1 ELSE 0 END) AS "month6"
FROM maintenance_request_service AS mt
WHERE request_date>='2024-01-01' AND request_date<'2025-12-01'
GROUP BY mt.state


UPDATE maintenance_request_service 
SET piece=my_table.piece FROM
(SELECT mrs.id, mrs.origin, mpl.name as piece FROM maintenance_request_service AS mrs
JOIN maintenance_schedule AS ms ON ms.id=SPLIT_PART(mrs.origin, ',', 2)::INTEGER
JOIN maintenance_planning_line AS mpl ON mpl.id=ms.line
WHERE mrs.origin is not null) 
AS my_table 
WHERE maintenance_request_service.id=my_table.id